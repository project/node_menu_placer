# Node Menu Placer

On sites with complicated menu structures, it can quickly become slow and confusing to place new content within the menu structure, or move existing content around.  Node Menu Placer works alongside the [Menu Link Weight module](https://www.drupal.org/project/menu_link_weight) to make it much easier and less error prone to place content in your menus.
![thumbnail.png](img/thumbnail.png)

No configuration is required.  The module will automatically show up on content with a menu option available.

## Requirements
* [Menu Link Weight module](https://www.drupal.org/project/menu_link_weight) 1.x

## Installation
Install via composer using command `composer require 'drupal/node_menu_placer:^2.0'`

## Maintainers
Initial development and ongoing maintenance of this project is supported by [Kalamuna](https://www.kalamuna.com).
