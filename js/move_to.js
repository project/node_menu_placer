/**
 * @file
 * Functionality for Move To buttons.
 */
((Drupal, once) => {
  function removeIfExists(context, selector) {
    const e = context.querySelector(selector);
    if (e !== null) e.remove();
  }

  // Named function to stop event propagation
  function stopEventPropagation(event) {
    event.stopImmediatePropagation();
  }

  // Function to attach the event handler to both the item and its handles
  function attachEventHandler(item, eventName) {
    item.addEventListener(eventName, stopEventPropagation, true);

    const handles = item.querySelectorAll('.tabledrag-handle');
    handles.forEach((handle) => {
      handle.addEventListener(eventName, stopEventPropagation, true);
    });
  }

  // Function to disable tabledrag handles for non-current items
  function disableTableDragHandles(menuLinkWeightReorder) {
    const items = menuLinkWeightReorder.querySelectorAll(
      'tr:not([data-drupal-selector="edit-menu-menu-link-weight-link-current"])',
    );

    items.forEach((item) => {
      const events = ['mousedown', 'touchstart', 'pointerdown'];
      events.forEach((eventName) => {
        attachEventHandler(item, eventName);
      });
    });
  }

  Drupal.behaviors.move_to = {
    attach(context) {
      // We don't need to make all of our changes each time a table row is dragged, or if there is no Menu Link option.
      if (
        (typeof context.matches === 'function' && context.matches('tr')) ||
        context.querySelector('[data-drupal-selector="edit-menu"]') === null
      ) {
        return;
      }
      const editMenu = context.querySelector(
        '[data-drupal-selector="edit-menu"]',
      );
      const editMenuMenuParent = context.querySelector(
        '[data-drupal-selector="edit-menu-menu-parent"]',
      );
      const editMenuMenuLinkWeight = context.querySelector(
        '[data-drupal-selector="edit-menu-menu-link-weight"]',
      );
      const menuLinkWeightReorder = context.querySelector(
        '[data-drupal-selector="menu-link-weight-reorder"]',
      );

      // Save the starting menu parent, so when switching between menus, the place is not lost.
      if (editMenu && !editMenu.hasAttribute('data-starting-menu-parent')) {
        editMenu.setAttribute(
          'data-starting-menu-parent',
          editMenuMenuParent.querySelector('option:checked').value,
        );
      }

      // Create menu choice fieldset (after removing one that might exist already).
      removeIfExists(context, '#menu-link-select-menu-wrapper');
      const weightWrapper = context.querySelector('#menu-link-weight-wrapper');

      if (weightWrapper) {
        weightWrapper.insertAdjacentHTML(
          'beforebegin',
          '<fieldset id="menu-link-select-menu-wrapper"><legend>Select menu</legend></fieldset>',
        );
        // A flag to track whether we are in the menu that has a selected option.
        let parentChecked = '';
        // Start with the last item in the select list and move backwards.
        let thisOption = editMenuMenuParent.querySelector('option:last-child');
        while (thisOption) {
          // If this is the currently selected option, we want to mark its upcoming parent as selected.
          if (thisOption.value === editMenuMenuParent.value) {
            parentChecked = 'checked';
          }
          // If this is an option that doesn't start with dashes, it is a menu name.
          if (thisOption.textContent.substring(0, 1) !== '-') {
            // Remove the <> from around the menu name, and save its value.
            const menuName = thisOption.textContent.substring(
              1,
              thisOption.textContent.length - 1,
            );
            const menuId = thisOption.value;
            const radioHtml = `<p><input type="radio" name="menu-link-select-menu" id="menu-link-select-menu-${menuId}" value="${menuId}" ${parentChecked} /><label for="menu-link-select-menu-${menuId}">${menuName}</label></p>`;
            // Create a radio button and label for this menu, checking it if needed.
            context
              .querySelector('#menu-link-select-menu-wrapper legend')
              .insertAdjacentHTML('afterend', radioHtml);
            // Unset the checked flag once we just marked this menu as checked.
            parentChecked = '';
          }
          thisOption = thisOption.previousElementSibling;
        }

        // Change the drupal menu parent select when a radio button is pressed.
        once(
          'menu-link-move-to',
          '#menu-link-select-menu-wrapper input',
          context,
        ).forEach((el) => {
          el.addEventListener('change', (event) => {
            // Start at the root of the menu that is being selected.
            let newParentId = event.currentTarget.value;
            // But if switching back to the starting menu, return to the starting parent.
            if (
              `${
                editMenu.getAttribute('data-starting-menu-parent').split(':')[0]
              }:` === event.currentTarget.value
            ) {
              newParentId = editMenu.getAttribute('data-starting-menu-parent');
            }
            // Switch to the new menu parent and trigger the menu link weight table change.
            editMenuMenuParent.value = newParentId;
            editMenuMenuParent.dispatchEvent(new Event('change'));
          });
        });

        // Remove the links provided by the menu_link_weight module, and add a select button to use instead.
        menuLinkWeightReorder
          .querySelectorAll('.menu-item__link')
          .forEach(function replaceLinkWithButton(item) {
            const buttonHTML = `<div class='menu-item__select'><span>${
              item.innerHTML
            }</span><span><button data-drupal-selector='${item
              .getAttribute('data-drupal-selector')
              .substring(
                44,
                80,
              )}'>Place<span class='visually-hidden'>section</span></button></span></div>`;

            item.insertAdjacentHTML('afterend', buttonHTML);
            item.parentNode.removeChild(item);
          });

        // When clicking the select buttons, move to the new parent item.
        const buttons = context.querySelectorAll('.menu-item__select button');
        if (buttons.length !== 0) {
          buttons.forEach((button) =>
            button.addEventListener('click', (event) => {
              event.preventDefault();
              // Get the menu name from the currently selected parent item option.
              const parentMenu = editMenuMenuParent.value.split(':')[0];
              // Get the menu item hash from the pressed link in the reorder table.
              const newHash = event.currentTarget.getAttribute(
                'data-drupal-selector',
              );
              // Change the value of the parent menu selector to the matching menu name and id hash.
              editMenuMenuParent.value = `${parentMenu}:menu_link_content:${newHash}`;
              editMenuMenuParent.dispatchEvent(new Event('change'));
            }),
          );
        }

        // Adapt the menu_link_weight fieldgroup so we can use it for parent selection as well as sibling order.
        editMenuMenuLinkWeight.querySelector(':scope > legend span').innerText =
          'Page location';
        // Change the help text to clarify the new interface.
        const help = editMenuMenuLinkWeight.querySelector(
          '[data-drupal-field-elements="description"]',
        );
        help.innerText =
          'Select a new page location by clicking "Place." Reorder the page by using the four-headed arrow to drag and drop.';
        // Move the help text to the top of the fieldgroup.
        editMenuMenuLinkWeight.insertBefore(
          help,
          editMenuMenuLinkWeight.querySelector(':scope > .fieldset__wrapper'),
        );
        // Move the table weight toggle to the bottom of the table since it is not important.
        menuLinkWeightReorder.after(
          editMenuMenuLinkWeight.querySelector(
            '.tabledrag-toggle-weight-wrapper',
          ),
        );
        // Disable tabledrag handles, so only the current page can be moved.  Do this by stopping the events from propagating, since removing listeners indiscriminately is hard in vanilla.
        disableTableDragHandles(menuLinkWeightReorder);

        // Use the currently selected parent to create breadcrumbs and a prominent title to indicate the current level.
        const currentOption =
          editMenuMenuParent.querySelector('option:checked');
        // Get the menu item title of the currently selected option.
        let currentParentTitle = currentOption.innerText.substring(
          currentOption.innerText.indexOf(' '),
        );
        // For menu root, use element text substring.
        if (
          currentOption.value.substring(currentOption.value.indexOf(':'))
            .length === 1
        ) {
          currentParentTitle = currentOption.innerText.slice(1, -1);
        }
        // Change the table heading to the current menu parent title.
        menuLinkWeightReorder.querySelector('thead th:first-child').innerText =
          currentParentTitle;

        // Create (or recreate) a breadcrumb section above the table for navigation upwards in the hierarchy.
        removeIfExists(context, '#menu-parent-breadcrumbs');
        menuLinkWeightReorder.insertAdjacentHTML(
          'beforebegin',
          '<ul id="menu-parent-breadcrumbs"></ul>',
        );
        const menuParentBreadcrumbs = document.getElementById(
          'menu-parent-breadcrumbs',
        );

        // We are going to start at the currently selected option in the dropdown and work backwards.
        thisOption = editMenuMenuParent.querySelector('option:checked');
        // Check that we are actually below the menu root in the hierarchy before creating the breadcrumbs.
        if (thisOption.innerText.indexOf('-') === 0) {
          // The number of dashes at the start of the text indicates the level of an item in the hierarchy.
          let currentLevel = thisOption.innerText.indexOf(' ');
          // We are going to iterate backwards from the selected option to get its parents.
          while (thisOption.previousElementSibling) {
            thisOption = thisOption.previousElementSibling;

            // For menu root, use element text substring.
            if (
              thisOption.value.substring(thisOption.value.indexOf(':'))
                .length === 1
            ) {
              const title = thisOption.innerText.substring(
                1,
                thisOption.innerText.length - 1,
              );
              const li = document.createElement('li');
              li.innerHTML = `<span>${title} &rsaquo;</span><span><button data-parent-option="${thisOption.value}">Move to <span class="visually-hidden">section</span></button></span>`;
              menuParentBreadcrumbs.prepend(li);
              break;
            } else {
              // Get the hierarchy level of the item we are parsing.
              const thisLevel = thisOption.innerText.indexOf(' ');
              // Check if the level of this item is lower than we've seen before.
              if (thisLevel < currentLevel) {
                // If so, get the new level, and use it to create the breadcrumb.
                currentLevel = thisLevel;
                const title = thisOption.innerText.substring(thisLevel + 1);
                const li = document.createElement('li');
                li.innerHTML = `<span>${'&nbsp;'.repeat(
                  thisLevel,
                )}${title} &rsaquo;</span><span><button data-parent-option="${
                  thisOption.value
                }">Move to <span class="visually-hidden">section</span></button></span>`;
                menuParentBreadcrumbs.prepend(li);
              }
            }
          }
        }
        // Now that the breadcrumb links have been created, allow them to be pressed to change the parent item input and refresh the table.
        const breadcrumbButtons = context.querySelectorAll(
          '#menu-parent-breadcrumbs button',
        );
        if (breadcrumbButtons.length !== 0) {
          breadcrumbButtons.forEach((breadcrumbButton) =>
            breadcrumbButton.addEventListener('click', (event) => {
              event.preventDefault();
              editMenuMenuParent.value =
                event.currentTarget.getAttribute('data-parent-option');
              editMenuMenuParent.dispatchEvent(new Event('change'));
            }),
          );
        }

        // Finally, remove the default drupal parent menu select input once our new interface has been successfully created.
        if (editMenuMenuParent) {
          editMenuMenuParent.style.display = 'none';
          context.querySelector(
            'label[for="edit-menu-menu-parent"]',
          ).style.display = 'none';
        }

        // Now that the interface has properly loaded, we can apply the css styles.
        editMenu.setAttribute('data-menu-helper-loaded', 'true');
      }
    },
  };
})(Drupal, once);
